/*!
 * thuynt
 * 
 * 
 * @author thuynt
 * @version 2.0.0
 * Copyright 2021. MIT licensed.
 */$(document).ready(function () {
  $("#form-password").click(function () {
    $(".form-content-info").toggleClass("show");
  });
  $(".btn-action-address").click(function (e) {
    $(".wrapper-popup").toggleClass("show");
    e.stopPropagation();
  });
  $(".inner-popup").click(function (e) {
    e.stopPropagation();
  });
  $(document).click(function () {
    $(".wrapper-popup").removeClass("show");
  });
  $(".btn-cancel").click(function () {
    $(".wrapper-popup").removeClass("show");
  });
});