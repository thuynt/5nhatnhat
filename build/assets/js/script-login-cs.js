/*!
 * thuynt
 * 
 * 
 * @author thuynt
 * @version 2.0.0
 * Copyright 2021. MIT licensed.
 */$(document).ready(function () {
  $("#btn-login-password").click(function () {
    $("#login-otp").hide();
    $("#login-password").show();
  });
});