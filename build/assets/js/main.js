$(document).ready(function () {
    function sliderBlock() {
        if (window.location.pathname == '/san-pham') {
            // slider product
            $('.slider-block').not('.slick-initialized').slick({
                slidesToShow: 4,
                slidesToScroll: 4,
                nextArrow: '<button type="button" class="slick-next"></button>',
                prevArrow: '<button type="button" class="slick-prev"></button>',
                dots: false,
                infinite: true,
                responsive: [{
                    breakpoint: 1201,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        arrows: false
                    }
                }, {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        arrows: false
                    }
                }, {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                        variableWidth: true
                    }
                }]
            });
        }
    }

    var resizeId;
    $(window).resize(function () {
        clearTimeout(resizeId);
        resizeId = setTimeout(function () {
            sliderBlock();
        });
    });
    sliderBlock();
});

var isMobileScreen = function () {
    return document.body.clientWidth < 992;
};

var app = {
    init: function () {
        this.slider();
        this.calcVH();
        // this.quickcart();
    },
    slick_on_mobile: function (slider, settings) {
        $(window).on('load resize', function () {
            if ($(window).width() > 991) {
                if (slider.hasClass('slick-initialized')) {
                    slider.slick('unslick');
                }
                return;
            }
            if (!slider.hasClass('slick-initialized')) {
                return slider.slick(settings);
            }
        });
    },
    slider: function () {
        if ($('.slider-product').length) {
            $('.slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.slider-nav'
            });
            $('.slider-nav').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                asNavFor: '.slider-for',
                dots: false,
                centerMode: true,
                focusOnSelect: true,
                variableWidth: true
            });
        }
        if ($('.slider-hero').length) {
            $('.slider-hero').not('.slick-initialized').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                fade: true,
                dots: true,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 5000
            });
        }

        /* slick only mobile */
        if ($('.ul-hot-products').length) {
            var slick_slider = $('.ul-hot-products');
            var settings_slider = {
                dots: true,
                arrows: false,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1
            };
            app.slick_on_mobile(slick_slider, settings_slider);
        }

        if ($('.slider-drug-cate').length) {
            $('.slider-drug-cate').not('.slick-initialized').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                fade: true,
                dots: true,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 5000,
                nextArrow: '<button type="button" class="slick-next"></button>',
                prevArrow: '<button type="button" class="slick-prev"></button>',
                responsive: [{
                    breakpoint: 992,
                    settings: {
                        arrows: false,
                        nextArrow: '',
                        prevArrow: ''
                    }
                }]
            });
        }

        if ($('.sider-pathology').length) {
            $('.sider-pathology').not('.slick-initialized').slick({
                slidesToShow: 3,
                slidesToScroll: 3,
                dots: true,
                infinite: true,
                arrows: false,
                responsive: [{
                    breakpoint: 992,
                    settings: {
                        dots: false,
                        arrows: false,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        centerMode: false,
                        variableWidth: true
                    }
                }]
            });
        }
    },
    closeMenuMobile: function () {
        $('.mobile-nav').removeClass('show');
        $('html, body').removeClass('open-accordion');
        $('.nav-mobile').removeClass('cancel-nav-mobile');
    },
    menuMobile: function () {
        $('.nav-mobile').click(function () {
            if (!$(this).hasClass('cancel-nav-mobile')) {
                $(this).addClass('cancel-nav-mobile');
                $('.mobile-nav').addClass('show');
                $('html, body').addClass('open-accordion');
            } else {
                $('.mobile-nav').removeClass('show');
                $('html, body').removeClass('open-accordion');
                $('.nav-mobile').removeClass('cancel-nav-mobile');
            }
        });
        $('.cancel-nav-mobile,.overlay-mobile-nav,.left-up-header,.wrap-logo,.hero-dashboard').click(function () {
            app.closeMenuMobile();
        });
        $('.card.no-sub-nav .card-header a').click(function () {
            app.closeMenuMobile();
        });
        $('.ul-products li').click(function () {
            app.closeMenuMobile();
        });
        app.accountDropdown();
    },
    accountDropdown: function () {
        $('#dropdownAccountHeader').on('show.bs.dropdown', function () {
            $('.mobile-nav').removeClass('show');
            $('.nav-mobile').removeClass('cancel-nav-mobile');
            $('html, body').addClass('open-accordion');
        });
        $('#dropdownAccountHeader').on('hide.bs.dropdown', function () {
            $('html, body').removeClass('open-accordion');
        });
    },
    slideToUnlockBtn: function (id, messages) {
        console.log('id: ', id);
        if ($(id).length) {
            $(id).slideToUnlock({
                lockText: messages,
                unlockText: ''
            });
        }
    },
    loginModalShow: function () {
        if ($('#loginModal').length) {
            $('#loginModal').on('shown.bs.modal', function (e) {
                app.slideToUnlockBtn('#sliderSendSms', 'trượt để nhận SMS');
            });
        }
    },
    resetPasswordModalShow: function () {
        if ($('#forgotPassword').length) {
            $('#forgotPassword').on('shown.bs.modal', function (e) {
                app.slideToUnlockBtn('#sliderSendNewPassword', 'trượt để nhận mật khẩu mới');
            });
        }
    },
    smsSliderShow: function () {
        if ($('.sms-slider').length) {
            app.slideToUnlockBtn('#sliderSendSms-gen', 'trượt để nhận SMS');
        }
        if ($('.code-slider').length) {
            app.slideToUnlockBtn('#sliderSendCode-gen', 'trượt để nhận mã xác thực');
        }
    },
    calcVH: function () {
        // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
        var vh = window.innerHeight * 0.01;
        // Then we set the value in the --vh custom property to the root of the document
        document.documentElement.style.setProperty('--vh', `${vh}px`);
    },
    preventClickInsideDropdown: function () {
        if ($('.dropdown-menu').length) {
            $('.dropdown-menu.mega-dropdown-menu').on('click', function (event) {
                // The event won't be propagated up to the document NODE and
                // therefore delegated events won't be fired
                console.log('prevent');
                event.stopPropagation();
            });
        }
    },
    preventScrollAccount: function () {
        if ($('#dropdownAccountDropdown').length) {
            $('#dropdownAccountDropdown').on('show.bs.dropdown', function () {
                $('body').addClass('fixedPosition');
                app.closeMenuMobile();
            });
            $('#dropdownAccountDropdown').on('hide.bs.dropdown', function () {
                $('body').removeClass('fixedPosition');
            });
        }
    },
    quickView: function () {
        if ($('#quickView').length) {
            $('#quickView').on('shown.bs.modal', function () {
                $('.slider-for').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: true,
                    asNavFor: '.slider-nav'
                });
                $('.slider-nav').slick({
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    asNavFor: '.slider-for',
                    dots: false,
                    arrows: false,
                    focusOnSelect: true
                });
            });
        }
    },
    quickcart: function () {
        if ($('#quickCartDropdown').length) {
            $('#quickCartDropdown').on('shown.bs.dropdown', function () {
                if ($('#badge-cart').text() == '')
                    $('#shopping_cart').hide();
                else
                    $('#shopping_cart').show();
                $('.prefectScroll').jScrollPane();
            });
        }
    },
    backToTop: function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('.back-to-top').fadeIn();
            } else {
                $('.back-to-top').fadeOut();
            }
        });
        $('.back-to-top').click(function () {
            $('html, body').animate({ scrollTop: 0 }, 600);
            return false;
        });
    },
    endOpenModal: function () {
        $('body').on('show.bs.modal', '.modal', function () {
            $('.modal:visible').removeClass('fade').modal('hide').addClass('fade');
        });
        // Handle the modal closing event
        $('.modal').on('hidden.bs.modal', function () {
            // Update NNECFE-114: Close popup, auto login vào website
            $('#register-popup').find('.sub-ct-form.step-3').each(function(){
                if ($(this).css('display') === 'block') {
                    window.location.reload();
                }
            });
            // Clear data login popup
            $('#login-popup input[name="telephone_email_login"]').val('');
            $('#login-popup .form-error.telephone_email_login').val('Email hoặc số điện thoại không được để trống.').hide(); // Error
            $('#login-popup').find('.slideText').text(slide_text_code_sms); // Reset câu thông báo Trượt để nhận mã xác thực qua SMS
            $('#slidePasswordLogin').hide();
            // password
            $('#login-popup input[name="telephone_password_login"]').val('');
            $('#login-popup .form-error.telephone_password_login').val('Mật khẩu không được để trống.').hide(); // Error
            $('#login-popup input[name="telephone_code_login"]').val('');
            $('#login-popup .form-error.telephone_code_login').val('Mật khẩu không được để trống.').hide(); // Error
            $('#login-popup input[name="email_password_login"]').val('');
            $('#login-popup .form-error.email_password_login').val('Mật khẩu không được để trống.').hide(); // Error
            $('#inputPasswordLogin').hide();
            // Show lại button Đăng nhập
            $('#login-popup .btn_login').show();

            // Clear data register popup
            $('#register-popup input[name="fullname_register"]').val('');
            $('#register-popup .form-error.fullname_register').val('Họ tên không được để trống.').hide(); // Error
            $('#register-popup input[name="telephone_email_register"]').val('');
            $('#register-popup .form-error.telephone_email_register').val('Số điện thoại hoặc email không được để trống.').hide(); // Error

            // password
            $('#register-popup input[name="telephone_code_register"]').val('');
            $('#register-popup .form-error.telephone_code_register').val('Mã xác thực không được để trống.').hide(); // Error
            $('#register-popup input[name="password_register"]').val('');
            $('#register-popup input[name="repeat_password_register"]').val('');
            $('#register-popup .telephone-group').hide();
            $('#register-popup .form-error.checkbox_agree').val('Bạn chưa đồng ý với điều khoản của chúng tôi.').hide(); // Error
            $('#register-modal .password-group').css({'display': 'none'});
            // Clear data reset popup
            $('#login-popup .btn-password').click();
            $('#reset-popup input[name="telephone_email_reset"]').val('');
            $('#reset-popup .form-error.telephone_email_reset').val('Email hoặc số điện thoại không được để trống.').hide(); // Error
            $('#slidePasswordReset').hide();
            // password
            $('#reset-popup .telephone_email_group').hide();
            $('#reset-popup input[name="telephone_email_password"]').val('');
            $('#reset-popup .form-error.telephone_email_password').val('Mật khẩu không được để trống.').hide(); // Error
        });

        $('#regToLogin').on('click', function () {
            // $('#regModal').modal('hide');
            $('.modal').modal('hide');
            setTimeout(() => {
                $('#loginModal').modal('show');
                $('#loginModal').removeClass('in').addClass('show');
                $('.modal-backdrop').removeClass('in').addClass('show');
            }, 500);
        });
        $('#loginToReg').on('click', function () {
            // $('#loginModal').modal('hide');
            $('.modal').modal('hide');
            setTimeout(() => {
                $('#regModal').modal('show');
                $('#regModal').removeClass('in').addClass('show');
                $('.modal-backdrop').removeClass('in').addClass('show');
                $('#register-modal').show(); // Hot fix lỗi đi từ màn hình giỏ hàng trên mobile click vào đăng ký ngay trên popup đăng nhập không mở được
            }, 500);
        });

        $('#doneReg').on('click', function () {
            // $('#regModal').modal('hide');
            $('.modal').modal('hide');
            setTimeout(() => {
                $('#successReg').modal('show');
            }, 500);
        });

        $('#loginToForgotPass, #loginToForgotPassForm').on('click', function () {
            $('.modal').modal('hide');
            setTimeout(() => {
                $('#forgotPassword').modal('show');
                $('#forgotPassword').removeClass('in').addClass('show');
                $('.modal-backdrop').removeClass('in').addClass('show');
            }, 500);
        });

        $('#resetPassToLogin').on('click', function () {
            $('.modal').modal('hide');
            setTimeout(() => {
                $('#loginModal').modal('show');
            }, 500);
        });
        $('#confirm-tel').on('click', function () {
            $('.modal').modal('hide');
            setTimeout(() => {
                $('#confirmTel').modal('show');
            }, 500);
        });
    },
    openSearchDesktop: function () {
        $('#seach-desktop').click(function () {
            $('.main-nav-and-search').toggleClass('expand-full-search');
        });
    },
    editable: function () {
        // toggle edit email - my account
        var ediable = 0;
        $('.edit-email').click(function () {
            var editParent = $(this).parent();

            if (!ediable) {
                editParent.removeClass('disabled-input');
                editParent.find('.form-control').removeAttr('disabled');
            } else {
                editParent.addClass('disabled-input');
                editParent.find('.form-control').attr('disabled', 'disabled');
            }
            ediable != ediable;
        });
    }
};

$(document).ready(function () {
    app.init();
    app.menuMobile();
    app.loginModalShow();
    app.resetPasswordModalShow();
    app.preventScrollAccount();
    app.quickView();
    app.preventClickInsideDropdown();
    app.quickcart();
    app.backToTop();
    // call modal
    app.endOpenModal();
    app.smsSliderShow();
    app.openSearchDesktop();
    app.accountDropdown();
    //app.editable();

    var resizeId;
    $(window).resize(function () {
        clearTimeout(resizeId);
        resizeId = setTimeout(function () {
            app.slider();
            app.calcVH();
            app.quickView();
        });
    });

    // UX (1)
    // Tạm bỏ theo yêu cầu của anh Minh ngày 07/04/2021
    /*
    if (window.location.pathname !== '/') {
        $('#headingTwo h5 a span').click(function () {
            var href = $(this).parent().attr('href');
            window.location.href = href;
        });
        $('#headingFour h5 a span').click(function () {
            var href = $(this).parent().attr('href');
            window.location.href = href;
        });
    }
     */
    $('.collapse').on('shown.bs.collapse', function () {
        $(this).prev().addClass('active');
    });

    $('.collapse').on('hidden.bs.collapse', function () {
        $(this).prev().removeClass('active');
    });

    // order-payment : payment method
    $('input[name=\'payment-method\']').change(function (e) {
        var selected_payment = $('input[name=\'payment-method\']:checked').attr('data-external');
        $('.show-external-payment').removeClass('show-external-payment');
        $('.' + selected_payment).addClass('show-external-payment');
    });

    $('input[name=\'bill\']').change(function (e) {
        var selected_bill = $('input[name=\'bill\']:checked').attr('data-external');
        $('.show-external-vat').removeClass('show-external-vat');
        $('.' + selected_bill).addClass('show-external-vat');
    });

    // sk-detail bai viet and sk-search-tag
    if ($('.is-sticky').length) {
        $('.is-sticky').sticksy({
            topSpacing: 150
        });
    }

    // cau hoi
    function toggleQuestion() {
        var $itemQuestion = $('.ul-list-text-question > li');
        if ($itemQuestion.length) {
            $('.ul-list-text-question > li').first().addClass('active');
            $('.ul-list-text-question > li .toggle-answer').click(function () {
                $(this).parent().parent().parent().toggleClass('active');
            });
        }
    }
    toggleQuestion();

    // Sync modal
    $('#cartSynModal').on('shown.bs.modal', function () {
        $('.prefectScroll').jScrollPane();
    });
    // Update NNECFE-116:
    // + Giỏ hàng có 1 sản phẩm --> bỏ scroll
    // + Giỏ hàng có nhiều sản phẩm, khi scroll trong popup đồng bộ giỏ hàng --> không được scroll current page
    // $('#cartSynModal').on('scroll touchmove mousewheel', function (e) {
    //     e.preventDefault();
    //     e.stopPropagation();
    //     return false;
    // });

    // select header
    $('.js-select-nn').select2({
        minimumResultsForSearch: Infinity
    });
    $('.select2-selection__rendered').hover(function() {
        $(this).removeAttr('title');
    });

    //
    $('.style-frequently-question').on('shown.bs.collapse', function () {
        $(this).prev().addClass('active');
    });

    $('.style-frequently-question').on('hidden.bs.collapse', function () {
        $(this).prev().removeClass('active');
    });
    // add to cart
    // $('.item-product:not(\'.out-of-stock\') .btn-add-to-cart').click(function () {
    //     var productCard = $(this).parent().parent().parent();
    //     var position = productCard.offset();
    //     var productImg = $(productCard).find('.img-product img');

    //     $('body').append('<div class="floating-cart"></div>');
    //     var cart = $('div.floating-cart');
    //     productImg.clone().appendTo(cart);
    //     $(cart).css({
    //         'top': position.top + 'px',
    //         'left': position.left + 'px',
    //         'opacity': '0.5'
    //     }).fadeIn('slow').addClass('moveToCart');
    //     setTimeout(function () {
    //         $('div.floating-cart').remove();
    //     }, 500);
    // });
    // add to card detail product
    $('.add-product-detail .btn-add-to-cart').click(function () {
        var productCard = $('.big-img-product:first');
        var position = productCard.offset();
        var productImg = $(productCard);

        $('body').append('<div class="floating-cart"></div>');
        var cart = $('div.floating-cart');
        productImg.clone().appendTo(cart);
        $(cart).css({
            'top': position.top + 'px',
            'left': position.left + 'px',
            'opacity': '0.5'
        }).fadeIn('slow').addClass('moveToCart');
        setTimeout(function () {
            $('div.floating-cart').remove();
        }, 500);
    });
    /*!
     * jQuery Cookie Plugin v1.4.1
     * https://github.com/carhartl/jquery-cookie
     *
     * Copyright 2006, 2014 Klaus Hartl
     * Released under the MIT license
     */
    (function (factory) {
        if (typeof define === 'function' && define.amd) {
            // AMD (Register as an anonymous module)
            define(['jquery'], factory);
        } else if (typeof exports === 'object') {
            // Node/CommonJS
            module.exports = factory(require('jquery'));
        } else {
            // Browser globals
            factory(jQuery);
        }
    }(function ($) {
        var pluses = /\+/g;
        function encode(s) {
            return config.raw ? s : encodeURIComponent(s);
        }
        function decode(s) {
            return config.raw ? s : decodeURIComponent(s);
        }
        function stringifyCookieValue(value) {
            return encode(config.json ? JSON.stringify(value) : String(value));
        }
        function parseCookieValue(s) {
            if (s.indexOf('"') === 0) {
                // This is a quoted cookie as according to RFC2068, unescape...
                s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
            }
            try {
                // Replace server-side written pluses with spaces.
                // If we can't decode the cookie, ignore it, it's unusable.
                // If we can't parse the cookie, ignore it, it's unusable.
                s = decodeURIComponent(s.replace(pluses, ' '));
                return config.json ? JSON.parse(s) : s;
            } catch (e) {
            }
        }
        function read(s, converter) {
            var value = config.raw ? s : parseCookieValue(s);
            return $.isFunction(converter) ? converter(value) : value;
        }
        var config = $.cookie = function (key, value, options) {
            // Write
            if (arguments.length > 1 && !$.isFunction(value)) {
                options = $.extend({}, config.defaults, options);
                if (typeof options.expires === 'number') {
                    var days = options.expires, t = options.expires = new Date();
                    t.setMilliseconds(t.getMilliseconds() + days * 864e+5);
                }
                return (document.cookie = [
                    encode(key), '=', stringifyCookieValue(value),
                    options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                    options.path ? '; path=' + options.path : '',
                    options.domain ? '; domain=' + options.domain : '',
                    options.secure ? '; secure' : ''
                ].join(''));
            }
            // Read
            var result = key ? undefined : {},
                // To prevent the for loop in the first place assign an empty array
                // in case there are no cookies at all. Also prevents odd result when
                // calling $.cookie().
                cookies = document.cookie ? document.cookie.split('; ') : [],
                i = 0,
                l = cookies.length;
            for (; i < l; i++) {
                var parts = cookies[i].split('='),
                    name = decode(parts.shift()),
                    cookie = parts.join('=');
                if (key === name) {
                    // If second argument (value) is a function it's a converter...
                    result = read(cookie, value);
                    break;
                }
                // Prevent storing a cookie that we couldn't decode.
                if (!key && (cookie = read(cookie)) !== undefined) {
                    result[name] = cookie;
                }
            }
            return result;
        };
        config.defaults = {};
        $.removeCookie = function (key, options) {
            // Must not alter options, thus extending a fresh object...
            $.cookie(key, '', $.extend({}, options, {expires: -1}));
            return !$.cookie(key);
        };
    }));
    // Store an array in jquery cookie
    var cookieFavorite = function (cookieName) {
        // when the cookie is saved the items will be a comma seperated string
        // so we will split the cookie by comma to get the original array
        var cookie = $.cookie(cookieName);
        // load the items or a new array if null
        var items = cookie ? cookie.split(/,/) : new Array();
        // return a object that we can use to access the array
        // while hiding access to the declared items array
        // this is called closures
        return {
            'add': function (val) {
                // add to the items
                items.push(val);
                // save the items to a cookie
                $.cookie(cookieName, items.join(','));
            },
            'remove': function (val) {
                var indx = items.indexOf(val);
                if (indx != -1) items.splice(indx, 1);
                $.cookie(cookieName, items.join(','));
            },
            'clear': function () {
                items = null;
                // clear the cookie
                $.cookie(cookieName, null);
            },
            'items': function () {
                // get all the items
                return items;
            }
        }
    }
    var list = new cookieFavorite('favorite_posts');
    // save to my favorite
    $('.save-my-favorite').click(function () {
        // checking the type of a null, empty or undefined var always returns 'undefined'
        if (!!$.cookie(COOKIE_USER_DATA)) {
            if ($(this).hasClass('saved')) {
                list.remove($(this).attr('post_id'));
                // call ajax to dislike post
                $.ajax({
                    url: base_url + '/account/delete/favorite/' + JSON.parse($.cookie(COOKIE_USER_DATA))[0].customer_id + '/post/' + $(this).attr('post_id'),
                    type: 'GET',
                    success: function (result, status, xhr) {
                        console.log(JSON.parse(result));
                    },
                    error: function (xhr, status, error) {
                        console.log(error);
                    }
                });
            } else {
                list.add($(this).attr('post_id'));
                // call ajax to like post
                $.ajax({
                    url: base_url + '/account/add/favorite/' + JSON.parse($.cookie(COOKIE_USER_DATA))[0].customer_id + '/post/' + $(this).attr('post_id'),
                    type: 'GET',
                    success: function (result, status, xhr) {
                        console.log(JSON.parse(result));
                    },
                    error: function (xhr, status, error) {
                        console.log(error);
                    }
                });
            }
            $(this).toggleClass('saved');
        } else {
            $('.modal').modal('hide');
            setTimeout(() => {
                showRegisterModal('login');
            }, 500);
        }
    });
    if (!!$.cookie(COOKIE_USER_DATA)) {
        $('.btn-dislike').click(function () {
            list.remove($(this).attr('post_id'));
            // call ajax to dislike post
            $.ajax({
                url: base_url + '/account/delete/favorite/' + JSON.parse($.cookie(COOKIE_USER_DATA))[0].customer_id + '/post/' + $(this).attr('post_id'),
                type: 'GET',
                success: function (result, status, xhr) {
                    console.log(JSON.parse(result));
                    window.location.reload();
                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });
        });
        if (((window.location.pathname.indexOf('suc-khoe') >= 0 || window.location.pathname.indexOf('bai-viet-chuyen-gia') >= 0 || window.location.pathname.indexOf('hoi-dap-chuyen-gia') >= 0) && window.location.pathname.indexOf('.html') >= 0) || window.location.pathname.indexOf('account/news') >= 0) {
            // call ajax to get all posts
            $.ajax({
                url: base_url + '/account/get/favorite/' + JSON.parse($.cookie(COOKIE_USER_DATA))[0].customer_id,
                type: 'GET',
                success: function (result, status, xhr) {
                    if (JSON.parse(result).data !== null) {
                        var result = JSON.parse(result).data.postId;
                        for (var i = 0; i < result.length; i++) {
                            $('.save-my-favorite[post_id="' + result[i] + '"]').addClass('saved');
                        }
                    }
                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });
        }
    }
    // <!-- only for my-account : date picker -->
    $( '.start_date_call' ).jqueryDatePicker({
        format:'dd-MM-yyyy',
        yearRangeVal:"-100:+50"
    });

    // view more mobile - detail promotion
    $('.view-more-detail-link').click(function () {
        $('.section-detail-promotion').toggleClass('show');
        if ($('.section-detail-promotion').hasClass('show')) {
            $(this).text('Thu gọn');
        } else {
            $(this).text('Xem thêm');
        }
    });
});

$(window).on('load', function () {});