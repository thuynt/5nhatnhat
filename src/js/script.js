$(document).ready(function() {
	$('.product button').click(function() {
        var offset = $(this).parent().offset();
        $(this).parent().clone().addClass('product-clone').css({
            'left' : offset.left + 'px',
            'top' : parseInt(offset.top-$(window).scrollTop()) + 'px',
            'width' :  $(this).parent().width() + 'px',
            'height' : $(this).parent().height() + 'px'
        }).appendTo($('.product').parent());

        var cart = $('#quickCartDropdown').offset();
        $('.product-clone').animate( { top: parseInt(cart.top-$(window).scrollTop()) + 'px', left: cart.left + 'px', 'height': '0px', 'width': '0px' }, 1800, function(){
            $(this).remove();
        });
    });
});