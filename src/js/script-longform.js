$(document).ready(function () {
    $("#form-ticket").on('click', 'input:checkbox', function() {
        $(this).closest('p').toggleClass('active', this.checked);
    });

    $("#btn-close-bar").click(function(){
        $(".longform-bar-fixbottom-2").addClass("shortbar-longform");
    });
});

var owl = $('.screenshot_slider').owlCarousel({
    loop: true,
    responsiveClass: true,
    nav: true,
    margin: 0,    
    autoplayTimeout: 4000,
    smartSpeed: 400,
    center: true,
    responsive: {
        0: {
            items: 5,
        },
        600: {
            items: 8
        },
        1200: {
            items: 7
        }
    }
});