<header>
    <div class="header">
        <div class="container h-100">
            <div class="row h-100">
                <div class="inner-header">
                    <h1 class="rs">
                        <a rel="noopener" href="https://nhatnhat.com/" class="logo">
                            <picture>
                                <source media="(max-width:768px)" width="167" height="30" srcset="assets/images/logo-mb.png">
                                <img class="lazy" alt="Dược Phẩm Nhất Nhất" width="299" height="56" data-original="assets/images/logo.png"/>
                            </picture>
                        </a>
                    </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="banner-header">
        <picture>
            <source media="(max-width:768px)" width="768" height="977" srcset="assets/images/banner-mb.jpg">
            <img class="lazy" alt="Dược Phẩm Nhất Nhất" width="1920" height="770" data-original="assets/images/banner.jpg"/>
        </picture>
    </div>
    <div class="container-fluid">
        <div class="box-ads-header">
            <div class="container h-100">
                <div class="row h-100 ">
                    <div class="inner-ads-header w-100 h-100">
                        <span class="btn-close" id="btn-close-ads"></span>
                        <div class="col-9 h-100">
                            <div class="row text-freeship">
                                <div class="text-1">BẠN ĐƯỢC HOÀN HÀNG</div>
                                <div class="text-2">nếu bao bì<br/> còn nguyên vẹn</div>
                                <div class="text-3">FREESHIP</div>
                            </div>
                            <form class="row ads-form" id=ads-form-header>
                                <div class="col-8 col-sm-7">
                                    <div class="row">
                                        <div class="form-group w-50">
                                            <input autocomplete="off" type="text" class="fullName" name="fullName" placeholder="Họ tên">
                                        </div>
                                        <div class="form-group w-50">
                                            <input autocomplete="off" type="number" class="phone" name="phone" placeholder="Số điện thoại" title="Số điện thoại không hợp lệ!">
                                        </div>
                                    </div>
                                    <div class="error error-bottom-mb">* Trường bắt buộc</div>
                                </div>
                                <div class="col-4 col-sm-5"> 
                                    <div class="row box-price-button">
                                        <div class="price-text"><span>285.000</span><small>VNĐ/HỘP</small></div>
                                        <button name="send-info" class="btn-scroll btn-send-info" id="btn-register">Đặt hàng</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-3 h-100">
                            <div class="row h-100 box-images-ads">
                                <span class="img-product-ads">
                                    <img width="282" height="282" class="w-100 lazy" alt="Đại tràng Nhất Nhất" data-original="assets/images/img-product1.png"/>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="cell-mobile">
    <div class="hotline-phone-ring-circle"></div>
    <a href="tel:0818122122" title="Hotline" class="btn_hotline_247_mobile">
        <img width="54" height="54" class="lazy Asset_45" data-original="https://dadaynhatnhat8.com/v2/landingpage/images/2021/daday_v4/Asset_45.png"  alt="Dược phẩm Nhất Nhất">
    </a>
</div>